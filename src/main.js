import React from 'react'
import ReactDOM from 'react-dom'
import createStore from './store/createStore'
import './styles/main.scss'
import setTokenToHeader from './auth/setTokenToHeader'
import { setCurrentUser } from './routes/User/modules/user'
let jwtDecode = require('jwt-decode')

// Store Initialization
// ------------------------------------
const store = createStore(window.__INITIAL_STATE__)
// Dev only
window.store = store
if (localStorage.jwtToken == 'undefined') {
  localStorage.clear()
}

if (localStorage.jwtToken) {
  setTokenToHeader(localStorage.jwtToken)
  store.dispatch(setCurrentUser(jwtDecode(localStorage.jwtToken)))
} else if (location.pathname.split('token=/')[1]) {
  localStorage.setItem('jwtToken', location.pathname.split('token=/')[1])
  setTokenToHeader(location.pathname.split('token=/')[1])
  store.dispatch(setCurrentUser(jwtDecode(location.pathname.split('token=/')[1])))
}
// Render Setup
// ------------------------------------
const MOUNT_NODE = document.getElementById('root')

let render = () => {
  const App = require('./components/App').default
  const routes = require('./routes/index').default(store)

  ReactDOM.render(
    <App store={store} routes={routes} />,
    MOUNT_NODE
  )
}

// Development Tools
// ------------------------------------
if (__DEV__) {
  if (module.hot) {
    const renderApp = render
    const renderError = (error) => {
      const RedBox = require('redbox-react').default

      ReactDOM.render(<RedBox error={error} />, MOUNT_NODE)
    }

    render = () => {
      try {
        renderApp()
      } catch (e) {
        console.error(e)
        renderError(e)
      }
    }

    // Setup hot module replacement
    module.hot.accept([
      './components/App',
      './routes/index',
    ], () =>
      setImmediate(() => {
        ReactDOM.unmountComponentAtNode(MOUNT_NODE)
        render()
      })
    )
  }
}

// Let's Go!
// ------------------------------------
if (!__TEST__) render()
