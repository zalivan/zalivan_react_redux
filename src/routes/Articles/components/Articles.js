import React from 'react'
import PropTypes from 'prop-types'
import RenderArticles from './RenderArticles'
import Select from 'react-select'
import './Articles.scss'
import ToogleOpen from "../../../components/ToggleOpen"
let _ = require('underscore')
import axios from 'axios'
import { API } from '../../../constants'
import { browserHistory } from 'react-router'

class Articles extends React.Component {
  componentWillMount () {
    axios({
      url: API + 'api/v1/articles/',
      method: 'get'
    }).then((response) => {
      this.setState({
        articles: response.data.data,
        tempArray: response.data.data
      })
    }).catch((errors) => {
      alert('errors from server gettArticles()', errors.message)
    })
   // this.setState({
   //   search: ''
   // })
  }

  componentDidMount () {
    this.props.getAllArticles()
    // const optionsTitles = this.props.articles.articles.articles.map(article => ({
    //   label: article.title,
    //   value: article.id
    // }))
    // this.setState({ optionsTitles: optionsTitles })
    axios({
      url: API + 'api/v1/articles/',
      method: 'get'
    }).then((response) => {
      this.setState({
        articles: response.data.data,
        tempArray: response.data.data
      })
    }).catch((errors) => {
      alert('errors from server getArticles()', errors.message)
    })
  }

  state = {
    selectedUsers: '',
    selectedTitles: '',
    selectedTags: '',
    search: '',
    tempArray: [],
    pageOfItems: [],
    optionsTitles: [],
    articles: [],
    isArticles: false
  }
  render () {
    let tempUsers = this.state.articles.map(article => ({
      label: article.username,
      value: article.id
    }))
    let optionsUsers = _.unique(tempUsers, function(item, label, a) {
      return item.label
    })

    const optionsTitles = this.state.articles.map(article => ({
      label: article.title,
      value: article.id
    }))
    let tempTags = this.state.articles.map(article => ({
      label: article.tag,
      value: article.id
    }))
    let optionsTags = _.unique(tempTags, function(item, label, a) {
      return item.label
    })
    let allArticlesSelector = (
      <div>
        <br />
        <div className='text-center'>
          <input
            className='form-control col-lg-5 center'
            placeholder='Search ...'
            // value={this.state.search || ''}
            onChange={this.handleChangeSearch}
            onKeyDown={this.searchText}
            ref='searchField'
          />
        </div>
      </div>
    )

    let filterSelector = (
      <div>
        <div className='select_wrapper'>
          <label>User
            <Select
              style={{ width: '200px' }}
              name='form-field-name'
              value={this.state.selectedUsers.value}
              clearable={true}
              onChange={this.handleChangeUsers}
              options={optionsUsers}
            />
          </label>
        </div>
        <div className='select_wrapper'>
          <label>Title
            <Select
              style={{ width: '200px' }}
              name='form-field-name'
              value={this.state.selectedTitles.value}
              onChange={this.handleChangeTitles}
              options={optionsTitles}
              clearable={true}
            />
          </label>
        </div>
        <div className='select_wrapper'>
          <label>Tag
            <Select
              style={{ width: '200px' }}
              name='form-field-name'
              value={this.state.selectedTags.value}
              onChange={this.handleChangeTags}
              options={optionsTags}
              clearable={true}
            />
          </label>
        </div>
        <button
          className='btn btn-dark filter_button'
          onClick={this.filterArticles}>
          Filter
        </button>
        <br />
        <button
          className='btn btn-dark filter_button'
          onClick={this.clearFilter}>
          Clear
        </button>
      </div>
    )
    return (
      <div style={{ margin: '50px auto 0' }} className='clearfix container'>
        <div className='clearfix'>
        <button onClick={this.props.toggleOpen} className='btn btn-dark toggle_button'>
          {this.props.isOpen ? 'Cancel' : 'Filter' }
        </button>
        </div>
        {this.props.isOpen ? filterSelector : allArticlesSelector }
        {/*{this.props.isOpen ? (this.state.tempArray.length ? <h2>No results...</h2>*/}
          {/*: <RenderArticles articles={this.state.tempArray} />) : ''}*/}
        {this.props.isOpen && <RenderArticles tempArray={this.state.tempArray} isArticles={this.state.isArticles} />}
        {!this.props.isOpen && <RenderArticles tempArray={this.state.tempArray} isArticles={this.state.isArticles} />}
      </div>
    )
  }

  handleChangeUsers = (selectedUsers) => {
    if (selectedUsers === null) {
      this.setState({ selectedUsers: '' })
    } else {
      this.setState({ selectedUsers })
    }
  }
  handleChangeTitles = (selectedTitles) => {
    if (selectedTitles === null) {
      this.setState({ selectedTitles: '' })
    } else {
      this.setState({ selectedTitles })
    }
  }
  handleChangeTags = (selectedTags) => {
    if (selectedTags === null) {
      this.setState({ selectedTags: '' })
    } else {
      this.setState({ selectedTags })
    }
  }

  handleChangeSearch = (ev) => {
    if (!ev.target.value) {
      this.setState({
        tempArray: this.state.articles
      })
      this.forceUpdate()
      browserHistory.push('/')
    }
    let text = ev.target.value.toLowerCase()
    let filterArray = this.state.articles.filter((article) => {
      if (~article.username.toLowerCase().indexOf(text) || ~article.tag.toLowerCase().indexOf(text) || ~article.title.toLowerCase().indexOf(text)) {
        return this
      }
    })
    if (filterArray.length) {
      this.setState({ search: ev.target.value, tempArray: filterArray, isArticles: true })
    } else {
      this.setState({ search: ev.target.value, tempArray: filterArray, isArticles: false })
    }
  }

  searchText = (ev) => {
    if (ev.keyCode === 8) {
      const text = this.refs.searchField.value.toLowerCase()
      let filterArray = this.state.articles.filter((article) => {
        if (~article.username.toLowerCase().indexOf(text) || ~article.tag.toLowerCase().indexOf(text) || ~article.title.toLowerCase().indexOf(text)) {
          return this
        }
      })
      if (filterArray.length) {
        this.setState({ search: ev.target.value, tempArray: filterArray, isArticles: true })
      } else {
        this.setState({ search: ev.target.value, tempArray: filterArray, isArticles: false })
      }
    }

  }

  filterArticles = () => {
    const tempUsername = this.state.selectedUsers.label
    const tempTitle = this.state.selectedTitles.label
    const tempTag = this.state.selectedTags.label
    let tempArticles = []
    let tempArticlesTitle = []
    let tempArticlesTag = []

    if (tempUsername) {
      tempArticles = this.state.articles.filter(article => {
        if (article.username === tempUsername) return article
      })
    } else { tempArticles = this.state.articles }
    if (tempTitle) {
      tempArticlesTitle = tempArticles.filter(article => {
        if (article.title === tempTitle) return article
      })
    } else { tempArticlesTitle = tempArticles }
    if (tempTag) {
      tempArticlesTag = tempArticlesTitle.filter(article => {
        if (article.tag === tempTag) return article
      })
    } else { tempArticlesTag = tempArticlesTitle }
    this.setState({
      tempArray: tempArticlesTag
    })
  }
  clearFilter = () => {
    this.setState({
      selectedTags: '',
      selectedTitles: '',
      selectedUsers: '',
      tempArray: this.state.articles
    })
  }
}

Articles.propTypes = {
  articles: PropTypes.object.isRequired,
  getAllArticles: PropTypes.func.isRequired,
  searchTextOnServer: PropTypes.func.isRequired
}

export default ToogleOpen(Articles)
