import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router'
import './Articles.scss'
import Image from 'react-image-file'
import { API } from '../../../constants'

export default class RenderOneArticle extends Component {
  render () {
    const { article } = this.props
    return (
      <div data-id={article.id}>
        <div className='clearfix'>
          <img height={150} src={API + `${article.image.url}`} />
          <br />
          <h2 className='news__title'>
            {article.title}
          </h2>
          <br />
        </div>
        <h3>
          #{article.tag}
        </h3>
        <p>
          {article.body}
        </p>
        Author: { ' ' }
        <Link to={'/user_page/' + article.user_id.toString()
        } activeClassName='page-layout__nav-item--active'>{article.username}</Link>
        <br />
        <div className='news__line' />
        <br />
      </div>
    )
  }
}
// RenderOneArticle.propTypes = {
//   article: PropTypes.object.isRequired
// }
