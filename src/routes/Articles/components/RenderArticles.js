import React from 'react'
import RenderOneArticle from './RenderOneArticle'
import PropTypes from 'prop-types'
let _ = require('underscore')
import Pagination from '../../../components/Pagination'

export default class RenderArticles extends React.Component {
  constructor () {
    super()
    this.state = {
      exampleItems: [],
      pageOfItems: [],
      tempArray: this.props,
      isArticles: true
    }
    this.onChangePage = this.onChangePage.bind(this)
  }

  componentDidMount () {
    this.setState({
      exampleItems: this.props.tempArray
  })
  }

  onChangePage (pageOfItems) {
    this.setState({ pageOfItems: pageOfItems })
  }

  render () {
    let articles = this.props.tempArray
    // this.state.pageOfItems = articles
    return (
      <div>
        <br />
        { !articles.length && <h2>No results...</h2> }
        {this.state.pageOfItems.map(article =>
          <RenderOneArticle key={article.id} article={article} />)
        }
        <div className='row'>
          <div className='center'>
            <Pagination items={this.props.tempArray} onChangePage={this.onChangePage} />
          </div>
        </div>
      </div>

    )
  }
}
