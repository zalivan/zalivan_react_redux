// ------------------------------------
// Constants
// ------------------------------------
import axios from 'axios'
import { API } from '../../../constants'

export const GET_SEARCH_ARTICLES = 'GET_SEARCH_ARTICLES'
export const GET_ALL_ARTICLES = 'GET_ALL_ARTICLES'
// export const COUNTER_DOUBLE_ASYNC = 'COUNTER_DOUBLE_ASYNC'

// ------------------------------------
// Actions
// ------------------------------------

export function getAllArticles () {
  return dispatch => {
    axios({
      url: API + 'api/v1/articles/',
      method: 'get'
    }).then((response) => {
      dispatch({
        type: GET_ALL_ARTICLES,
        payload: response.data.data
      })
    }).catch((errors) => {
      alert('errors from server gettArticles()', errors.message)
    })
  }
}

export function searchTextOnServer (text) {
  return dispatch => {
    axios({
      url: API + 'api/v1/articles/search?text=' + text,
      method: 'get',
      data: {
        text: text
      }
    }).then((response) => {
      dispatch({
        type: GET_SEARCH_ARTICLES,
        payload: response.data.data
      })
    }).catch((errors) => {
      alert(errors.message)
    })
  }
}
/*  This is a thunk, meaning it is a function that immediately
    returns a function for lazy evaluation. It is incredibly useful for
    creating async actions, especially when combined with redux-thunk! */

export const actions = {
  getAllArticles,
  searchTextOnServer
}

// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [GET_ALL_ARTICLES]: (state, action) => {
    return Object.assign({}, state, { articles: action.payload })
  },
  [GET_SEARCH_ARTICLES]: (state, action) => {
    return Object.assign({}, state, { articles: action.payload })
  }
}

// ------------------------------------
// Reducer
// ------------------------------------
// const initialState = articles
//
const initialState = { articles: [] }
export default function articlesReducer (state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}
