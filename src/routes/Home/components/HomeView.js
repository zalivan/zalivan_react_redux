import React from 'react'
import DuckImage from '../assets/Duck.jpg'
import './HomeView.scss'
import { browserHistory } from 'react-router'

export const HomeView = () => (
  <div>
    {browserHistory.push('/')}
  </div>
)

export default HomeView
