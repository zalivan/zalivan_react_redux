// ------------------------------------
// Constants
// ------------------------------------
// import axios from 'axios'
// import { articles } from '../../../fixtures'

// export const GET_ARTICLES = 'GET_ALL_ARTICLES'
// export const COUNTER_DOUBLE_ASYNC = 'COUNTER_DOUBLE_ASYNC'

// ------------------------------------
// Actions
// ------------------------------------
// export function showAlert () {
//   return {
//     type    : SHOW_ALERT
//   }
// }

// export function getArticles (userId) {
//   return dispatch => {
//     axios({
//       url: 'http://localhost:3001/users/' + userId,
//       // url: 'http://6457552b.ngrok.io/users/' + userId,
//       method: 'get',
//       data: {
//         user_id: userId
//       }
//     }).then((response) => {
//       console.log('Data from server for UserPage', response.data.data)
//       dispatch({
//         type: GET_ARTICLES,
//         payload: response.data.data
//       })
//     }).catch((errors) => {
//       console.log('Error from server', errors)
//     })
//   }
// }
/*  This is a thunk, meaning it is a function that immediately
    returns a function for lazy evaluation. It is incredibly useful for
    creating async actions, especially when combined with redux-thunk! */

// export const doubleAsync = () => {
//   return (dispatch, getState) => {
//     return new Promise((resolve) => {
//       setTimeout(() => {
//         dispatch({
//           type    : COUNTER_DOUBLE_ASYNC,
//           payload : getState().counter
//         })
//         resolve()
//       }, 200)
//     })
//   }
// }

export const actions = {
  // getArticles
}

// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
  // [GET_ARTICLES]: (state, action) => {
  //   console.log(action.payload)
  //   return Object.assign({}, state, { articles: action.payload })
  // }
  // [COUNTER_DOUBLE_ASYNC] : (state, action) => state * 2
}

//   [GET_ARTICLES]: (state, action) => {
//   // console.log(action.payload)
//   return Object.assign({}, state, { articles: action.payload })
// },

// ------------------------------------
// Reducer
// ------------------------------------
// const initialState = articles
//
const initialState = { user: {} }
export default function counterReducer (state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}
