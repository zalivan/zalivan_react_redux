// ------------------------------------
// Constants
// ------------------------------------
import axios from 'axios'
import { API } from '../../../constants'
import { browserHistory } from 'react-router'
// import { articles } from '../../../fixtures'

export const GET_ARTICLES = 'GET_ALL_ARTICLES'
export const GET_USER = 'GET_USER'
export const DELETE_ARTICLE = 'DELETE_ARTICLE'
// export const COUNTER_DOUBLE_ASYNC = 'COUNTER_DOUBLE_ASYNC'

// ------------------------------------
// Actions
// ------------------------------------
export function getArticles (userId) {
  return dispatch => {
    axios({
      url: API + 'api/v1/articles/' + userId,
      // url: 'http://6457552b.ngrok.io/users/' + userId,
      method: 'get',
      data: {
        user_id: userId
      }
    }).then((response) => {
      getUser(userId)
      dispatch({
        type: GET_ARTICLES,
        payload: response.data.data
      })
    }).catch((errors) => {
    })
  }
}

export function deleteArticle (articleId) {
  return dispatch => {
    axios({
      url: API + 'api/v1/articles/' + articleId,
      method: 'delete',
      data: {
        id: articleId
      }
    }).then((response) => {
      dispatch({
        type: DELETE_ARTICLE,
        payload: response.data.data
      })
      browserHistory.push('/')
    }).catch((errors) => {
      alert(errors.message)
    })
  }
}

export function editUserData (id, username, email, image) {
  let formData = new FormData()
  formData.append('id', id)
  formData.append('email', email)
  formData.append('username', username)
  formData.append('image', image)
  formData.append('profile_image', null)
  formData.append('provider', null)
  return dispatch => {
    axios({
      url: API + 'users/' + id,
      method: 'put',
      data: formData
    }).then((response) => {
      localStorage.clear()
      localStorage.setItem('jwtToken', response.data.data)
      browserHistory.push('/my_account/' + id)
    }).catch((errors) => {
      console.log('Error from server', errors)
    })
  }
}

export function getUser (userId) {
  return dispatch => {
    axios({
      url: API + 'users/' + userId,
      method: 'get',
      data: {
        user_id: userId
      }
    }).then((response) => {
      dispatch({
        type: GET_USER,
        payload: response.data.data
      })
    }).catch((errors) => {
      console.log('Error from server', errors)
    })
  }
}
/*  This is a thunk, meaning it is a function that immediately
    returns a function for lazy evaluation. It is incredibly useful for
    creating async actions, especially when combined with redux-thunk! */

export const actions = {
  getArticles,
  getUser,
  deleteArticle,
  editUserData
}

// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [GET_ARTICLES]: (state, action) => {
    return Object.assign({}, state, { articles: action.payload })
  },
  [GET_USER]: (state, action) => {
    return Object.assign({}, state, { user: action.payload })
  },
  [DELETE_ARTICLE]: (state, action) => {
    return Object.assign({}, state, { articles: action.payload })
  },
}

// ------------------------------------
// Reducer
// ------------------------------------
// const initialState = articles
//
const initialState = {
  articles: [],
  user: {}
}
export default function myAccountReducer (state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}
