import React from 'react'
import './MyAccount.scss'
import { API } from '../../../constants'
let jwtDecode = require('jwt-decode')

export default class RenderArticlesOfUser extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      isEdit: this.props.isEdit,
      username: jwtDecode(localStorage.jwtToken).username,
      user: this.props.user
    }
  }

  render () {
    let userObject = jwtDecode(localStorage.jwtToken)
    let user = this.props.user
    let image = ''
    if (userObject.provider === 'twitter') {
      image = userObject.profile_image
    } else if (userObject.provider === 'google') {
      image = userObject.profile_image
    } else if (userObject.provider === 'null') {
      image = API + userObject.image.url
    } else {
      image = API + userObject.image.url
    }
    return (
      <div className='user'>
        <img
          src={image}
          className='user__avatar'
        />
        <div className='user__data'>
          <h3>username: <b style={{color: 'brown'}}>{user.username}</b></h3>
          <h2>email: <b style={{color: 'brown'}}>{user.email}</b></h2>
        </div>
      </div>
    )
  }
}

// RenderOneArticleOfUser.propTypes = {
//   articles: PropTypes.array.isRequired
// }
