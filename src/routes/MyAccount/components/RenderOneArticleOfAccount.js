import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Image from 'react-image-file'
import { API } from '../../../constants'
let jwtDecode = require('jwt-decode')
import './MyAccount.scss'

export default class RenderOneArticleOfAccount extends Component {
  render () {
    const article = this.props.article
    if (localStorage.jwtToken) {
    }
    let isDelete = (+jwtDecode(localStorage.jwtToken).user_id === +location.pathname.split('user_page/')[1])
    return (
      <div data-id={article.id}>
        <h2>
          {article.title}
          <br />
        </h2>
        <h3>
          {article.tag}
        </h3>
        <p>
          {article.body}
        </p>
        {/*<Image height={150} file={API + `${article.image.url}`} className='center-block' />*/}
        <img style={{ height: '150px' }} src={API + `${article.image.url}`} className='center-block article__image' />
        <br />
        <button
          data-id={article.id}
          onClick={this.handleDeleteArticle}
          className='btn btn-outline-danger article__image'
        >
          Delete
        </button>
        <br />
        <div className='news__line' />
        <br />
      </div>
    )
  }
  handleDeleteArticle = (ev) => {
    const articleId = +ev.target.getAttribute('data-id')
    this.props.deleteArticle(articleId)
  }
}
//
// RenderOneArticleOfAccount.propTypes = {
//   article: PropTypes.object.isRequired,
//   deleteArticle: PropTypes.func.isRequired
// }
