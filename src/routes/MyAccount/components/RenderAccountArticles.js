import React from 'react'
import RenderOneArticleOfAccount from './RenderOneArticleOfAccount'
import PropTypes from 'prop-types'
let _ = require('underscore')
import Pagination from '../../../components/Pagination'
import axios from 'axios'
import { API } from '../../../constants'
let jwtDecode = require('jwt-decode')


export default class RenderAccountArticles extends React.Component {
  constructor () {
    super()
    this.state = {
      exampleItems: this.props || [],
      pageOfItems: []
    }
    this.onChangePage = this.onChangePage.bind(this)
  }
  componentWillMount () {
    axios({
      url: API + 'api/v1/articles/' + +jwtDecode(localStorage.jwtToken).user_id,
      method: 'get',
      data: {
        user_id: +jwtDecode(localStorage.jwtToken).user_id
      }
    }).then((response) => {
       this.setState({
        // type: GET_ARTICLES,
         exampleItems: response.data.data
      })
    }).catch((errors) => {
    })
  }
  onChangePage (pageOfItems) {
    this.setState({ pageOfItems: pageOfItems })
  }
  render () {
    const idAccount = +jwtDecode(localStorage.jwtToken).user_id
    const articles = this.state.exampleItems
    return (
      <div>
        <br />
        {
          this.state.pageOfItems &&
            <div>
              <h2 style={{color: 'grey', marginBottom: '20px'}}>News</h2>
              <div className='news__line' />
              {this.state.pageOfItems.map(article =>
                <RenderOneArticleOfAccount key={article.id} article={article}
                                           deleteArticle={this.props.articles.deleteArticle}
                />)
              }
              <div className='row'>
                <div className='center'>
                  <Pagination items={articles} onChangePage={this.onChangePage} className='center' />
                </div>
              </div>
            </div>
        }
        {
          !this.state.pageOfItems &&
            <div>
              <h2 style={{color: 'grey', marginBottom: '20px'}}>You are not having news</h2>
            </div>
        }
      </div>
    )
  }
}

// RenderAccountArticles.propTypes = {
//   articles: PropTypes.array.isRequired
// }
