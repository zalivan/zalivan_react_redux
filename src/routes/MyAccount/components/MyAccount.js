import React from 'react'
import PropTypes from 'prop-types'
import RenderAccountArticles from './RenderAccountArticles'
import MyAccountData from './MyAccountData'
import EditUser from './EditUser'
import ToogleOpen from '../../../components/ToggleOpen'
import axios from 'axios'
import { API } from '../../../constants'
import { browserHistory } from 'react-router'

class Users extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      image: '',
      isEdit: true,
      user: {}
    }
  }

  isEditFunc (id) {
    this.setState({
      isEdit: !this.state.isEdit
    })
    this.browserPush(id)
  }

  browserPush = (id) => {
    this.props.getUser(id)
  }

  componentWillMount () {
    let arrLocation = +location.pathname.split('/')[2]
    // this.props.getUser(arrLocation)

  }

  componentDidMount () {
    let arrLocation = +location.pathname.split('/')[2]
    this.props.getUser(arrLocation)
    this.props.getArticles(arrLocation)
    axios({
      url: API + 'users/' + arrLocation,
      method: 'get',
      data: {
        user_id: arrLocation
      }
    }).then((response) => {
      this.setState({
        // type: GET_USER,
        user: response.data.data
      })
    }).catch((errors) => {
      console.log('Error from server', errors)
    })
  }

  render () {
    return (
      <div style={{ margin: '0 auto' }} >
        <MyAccountData user={this.state.user} isEdit={this.state.isEdit} />
        <button
          className='btn btn-dark user'
          onClick={this.props.toggleOpen}
        >
          {
            this.props.isOpen ? 'Show news' : 'Edit user'
          }
        </button>
        {
          this.props.isOpen &&
          <div>
            <EditUser articles={this.props} isEdit={this.isEditFunc.bind(this)} />
          </div>
        }
        {
          !this.props.isOpen &&
          <div>
            <RenderAccountArticles articles={this.props} />
          </div>
        }
      </div>
    )
  }
}

export default ToogleOpen(Users)

Users.propTypes = {
  getArticles: PropTypes.func.isRequired,
  getUser: PropTypes.func.isRequired
}
