import React from 'react'
import PropTypes from 'prop-types'
import RenderArticlesOfUser from './RenderUser'
import { Link } from 'react-router'
import ToogleOpen from '../../../components/ToggleOpen'
import './MyAccount.scss'
let jwtDecode = require('jwt-decode')

class EditUser extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      user_id: this.props.articles.user.user_page.user.id || jwtDecode(localStorage.jwtToken).user_id,
      username: jwtDecode(localStorage.jwtToken).username,
      email: jwtDecode(localStorage.jwtToken).email
    }
  }
  render () {
    let userObject = jwtDecode(localStorage.jwtToken)
    const user = this.props.articles.user.user_page.user
    return (
      <div style={{ margin: '0 auto' }} >
        <div>
          <form encType='multipart/form-data' method='post'>
            <label>
              Username
              <input
                className='form-control edit_form col-lg-12 center'
                defaultValue={this.state.username}
                onChange={this.handleError}
                ref='changeName'
              />
            </label>
            <br />
            <label>
              Email
              <input
                className="form-control edit_form col-lg-12 center"
                defaultValue={this.state.email}
                onChange={this.handleError}
                ref='changeEmail'
              />
            </label>
            <br />
            <label>Image
              <br />
              <input
                type='file'
                name='photo'
                className='form-control'
                ref='changeImage'
                onChange={this.handleError}
                accept='image/*'
              />
            </label>
            <br />
            <br />
            <button
              className='btn btn-outline-dark'
              type='submit'
              onClick={this.handleEditUser}>
              Edit
            </button>
          </form>
        </div>
      </div>
    )
  }
  handleError = (ev) => {
    ev.target.classList.remove('error')
  }
  handleEditUser = (ev) => {
    const Username = this.refs.changeName
    const Email = this.refs.changeEmail
    ev.preventDefault()
    const id = this.state.user_id
    const username = Username.value
    const email = Email.value
    const image = this.refs.changeImage.files[0]

    this.setState({
      username: username,
      emails: email
    })

    if ((username.trim() === '') || (username.replace(/\s/g, '') === '')) {
      Username.classList.add('error')
    } else if ((email.trim() === '') || (email.replace(/\s/g, '') === '') || !(/@/.test(email))) {
      Email.classList.add('error')
    } else {
      this.props.isEdit(id)
      this.props.articles.editUserData(id, username, email, image)
    }
  }
}

export default ToogleOpen(EditUser)

EditUser.propTypes = {}
