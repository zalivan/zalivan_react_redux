import React from 'react'
import PropTypes from 'prop-types'
import './User.scss'
import ToogleOpen from '../../../components/ToggleOpen'
import GoogleLogin from 'react-google-login'
import { Link } from 'react-router'
let jwtDecode = require('jwt-decode')
import axios from 'axios'
import { API, API_HOME } from '../../../constants'
import { browserHistory } from 'react-router'

class User extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      username: '',
      email: '',
      password: '',
      passwordConfirm: '',
      alert: ''
    }
  }

  render () {
    return (
      <div>
        <div className='clearfix container'>
          {this.state.alert && <div className='alert alert-danger'>{this.state.alert}</div>}
        </div>
        <div className='text-center'>
          <form className='col-lg-4 center' autoComplete='off'>
            <div className='form-group'>
              <label>Username</label>
              <input
                type='text'
                onChange={this.setUsername}
                className='form-control fields__rel'
                placeholder='Username'
                autoComplete='off'
                ref='createUsername'
              />
            </div>
            <div className='form-group'>
              <label>Email</label>
              <input
                type='email'
                onChange={this.setEmail}
                className='form-control fields__rel'
                placeholder='Email'
                autoComplete='off'
                ref='createEmail'
              />
            </div>
            <div className='form-group'>
              <label>Password</label>
              <input
                type='password'
                onChange={this.setPassword}
                className='form-control fields__rel'
                placeholder='Password'
                autoComplete='off'
                ref='createPassword'
              />
            </div>
            <div className='form-group'>
              <label>Confirm Password</label>
              <input
                type='password'
                onChange={this.setPasswordConfirm}
                className='form-control fields__rel'
                placeholder='Password'
                autoComplete='off'
                ref='createPasswordConfirm'
              />
            </div>
            <label>Image
              <br />
              <input
                type='file'
                name='photo'
                className='form-control'
                ref='createImage'
                onChange={this.changeImage}
                accept='image/*'
              />
            </label>
            <div>
              <button
                type='submit'
                onClick={this.onSignUp}
                className='btn btn-primary'>
                SignUp
              </button>
            </div>
            <br />
          </form>
        </div>
      </div>
    )
  }

  setUsername = (ev) => {
    ev.target.classList.remove('error')
    this.setState({
      username: ev.target.value,
      alert: ''
    })
  }

  setEmail = (ev) => {
    ev.target.classList.remove('error')
    this.setState({
      email: ev.target.value,
      alert: ''
    })
  }

  setPassword = (ev) => {
    ev.target.classList.remove('error')
    this.setState({
      password: ev.target.value,
      alert: ''
    })
  }

  setPasswordConfirm = (ev) => {
    ev.target.classList.remove('error')
    this.setState({
      passwordConfirm: ev.target.value,
      alert: ''
    })
  }

  onSignUp = (ev) => {
    const Username = this.refs.createUsername
    const Email = this.refs.createEmail
    const Password = this.refs.createPassword
    const PasswordConfirm = this.refs.createPasswordConfirm
    const Image = this.refs.createImage

    ev.preventDefault()
    const username = Username.value
    const email = Email.value
    const password = Password.value
    const passwordConfirm = PasswordConfirm.value
    const image = Image.files[0]

    if ((username.trim() === '') || (username.replace(/\s/g, '') === '')) {
      Username.classList.add('error')
      this.setState({
        alert: 'Enter correct Username!'
      })
    } else if ((email.trim() === '') || (email.replace(/\s/g, '') === '') || !(/@/.test(email))) {
      Email.classList.add('error')
      this.setState({
        alert: 'Enter correct email!'
      })
    } else if ((password.trim() === '') || (password.replace(/\s/g, '') === '') || (password.trim().length < 6)) {
      Password.classList.add('error')
      this.setState({
        alert: 'Password length must be at least 6 characters!'
      })
    } else if ((passwordConfirm.trim() === '') || (passwordConfirm.replace(/\s/g, '') === '')) {
      PasswordConfirm.classList.add('error')
    } else if (password !== passwordConfirm) {
      Password.classList.add('error')
      PasswordConfirm.classList.add('error')
      this.setState({
        alert: 'Passwords do not match!'
      })
    } else if (!image) {
      Image.classList.add('error')
      this.setState({
        alert: 'Please, add image to your news.'
      })
    } else {
      console.log(image)
      this.props.addUser(this.state.username, this.state.email, this.state.password, this.state.passwordConfirm, image)
    }
  }
}
export default ToogleOpen(User)
