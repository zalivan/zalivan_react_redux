import React from 'react'
import PropTypes from 'prop-types'
import './User.scss'
import ToogleOpen from '../../../components/ToggleOpen'
import UserSignUp from './UserSignUp'
import UserLogin from './UserLogin'

class User extends React.Component {
  constructor (props) {
    super(props)
  }

  render () {
    return (
      <div>
        <div className='clearfix container'>
          <button onClick={this.props.toggleOpen} className='btn btn-dark toggle_button' >
            {this.props.isOpen ? 'Log in' : 'Register' }
          </button>
        </div>
        {this.props.isOpen ? <UserSignUp {...this.props} /> : <UserLogin {...this.props} /> }
      </div>
    )
  }

  setUsername = (ev) => {
    ev.target.classList.remove('error')
    this.setState({
      username: ev.target.value,
      alert: ''
    })
  }

  setEmail = (ev) => {
    ev.target.classList.remove('error')
    this.setState({
      email: ev.target.value,
      alert: ''
    })
  }

  setPassword = (ev) => {
    ev.target.classList.remove('error')
    this.setState({
      password: ev.target.value,
      alert: ''
    })
  }

  setPasswordConfirm = (ev) => {
    ev.target.classList.remove('error')
    this.setState({
      passwordConfirm: ev.target.value,
      alert: ''
    })
  }

  onSignUp = (ev) => {
    const Username = this.refs.createUsername
    const Email = this.refs.createEmail
    const Password = this.refs.createPassword
    const PasswordConfirm = this.refs.createPasswordConfirm

    ev.preventDefault()
    const username = Username.value
    const email = Email.value
    const password = Password.value
    const passwordConfirm = PasswordConfirm.value

    if ((username.trim() === '') || (username.replace(/\s/g, '') === '')) {
      Username.classList.add('error')
      this.setState({
        alert: 'Enter correct Username!'
      })
    } else if ((email.trim() === '') || (email.replace(/\s/g, '') === '') || !(/@/.test(email))) {
      Email.classList.add('error')
      this.setState({
        alert: 'Enter correct email!'
      })
    } else if ((password.trim() === '') || (password.replace(/\s/g, '') === '') || (password.trim().length < 6)) {
      Password.classList.add('error')
      this.setState({
        alert: 'Password length must be at least 6 characters!'
      })
    } else if ((passwordConfirm.trim() === '') || (passwordConfirm.replace(/\s/g, '') === '')) {
      PasswordConfirm.classList.add('error')
    } else if (password !== passwordConfirm) {
      Password.classList.add('error')
      PasswordConfirm.classList.add('error')
      this.setState({
        alert: 'Passwords do not match!'
      })
    } else {
      this.props.addUser(this.state.username, this.state.email, this.state.password, this.state.passwordConfirm)
    }
  }
}
export default ToogleOpen(User)
