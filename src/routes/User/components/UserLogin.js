import React from 'react'
import PropTypes from 'prop-types'
import './User.scss'
import ToogleOpen from '../../../components/ToggleOpen'
import GoogleLogin from 'react-google-login'
import axios from 'axios'
import { API, API_HOME } from '../../../constants'
import { browserHistory } from 'react-router'

class User extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      credentials: { email: '', password: '' },
      alert: ''
    }
  }

  render () {
    return (
      <div>
        <div className='clearfix container'>
          {this.state.alert && <div className='alert alert-danger'>{this.state.alert}</div>}
        </div>
        <div className='text-center'>
          <form className='col-lg-5 center'>
            <div className='form-group'>
              <label>Email</label>
              <input
                type='email'
                name='email'
                onChange={this.onChange}
                className='form-control fields__rel'
                placeholder='Email'
                ref='setEmail'
                autoComplete='off'
              />
            </div>
            <div className='form-group'>
              <label>Password</label>
              <input
                type='password'
                name='password'
                onChange={this.onChange}
                className='form-control fields__rel'
                placeholder='Password'
                ref='setPassword'
                autoComplete='off'
              />
            </div>
            <div>
              <button
                type='submit'
                onClick={this.onLogIn}
                className='btn btn-primary'>
                Login
              </button>
            </div>
            <br />
            <div>
              <a href={API + 'auth/twitter'} className='btn btn-dark' style={{ marginBottom: '20px' }}>Twitter</a>
              <br />
              <GoogleLogin
                clientId='139407652676-tps0jhhrpnq8ae1ahcd7pkce280popkr.apps.googleusercontent.com'
                buttonText='Google'
                className='btn btn-dark'
                onSuccess={this.responseGoogle}
                onFailure={this.responseNotGoogle}
              />
            </div>
          </form>
        </div>
      </div>
    )
  }

  responseGoogle = (result) => {
    console.log('result from google', result.profileObj)
    axios({
      url: API + 'users',
      method: 'post',
      data: {
        result: result.profileObj,
        provider: 'google'
      }
    }).then((response) => {
      console.log(response)
      localStorage.setItem('jwtToken', response.data.data)
      browserHistory.push('/')
    }).catch((errors) => {
      console.log('Error from server', errors)
    })
  }

  onLogIn = (ev) => {
    const Email = this.refs.setEmail
    const Password = this.refs.setPassword
    ev.preventDefault()
    const emailLogIn = Email.value
    const passwordLoIn = Password.value
    if ((emailLogIn === '') || (emailLogIn.replace(/\s/g, '') === '') || !(/@/.test(emailLogIn))) {
      Email.classList.add('error')
      this.setState({
        alert: 'Enter correct email!'
      })
    } else if ((passwordLoIn === '') || (passwordLoIn.replace(/\s/g, '') === '') || (passwordLoIn.trim().length < 6)) {
      Password.classList.add('error')
      this.setState({
        alert: 'Password length must be at least 6 characters!!'
      })
    } else {
      this.props.logIn(emailLogIn, passwordLoIn)
    }
  }

  responseNotGoogle = (ev) => {
    ev.target.classList.add('error')
    this.setState({
      alert: 'You can not register using google!!'
    })
  }

  onChange = (ev) => {
    ev.target.classList.remove('error')
    const field = ev.target.name
    const credentials = this.state.credentials
    credentials[field] = ev.target.value
    return this.setState({ credentials: credentials })
  }
}
export default ToogleOpen(User)
