// ------------------------------------
// Constants
// ------------------------------------
import axios from 'axios'
import setTokenToHeader from '../../../auth/setTokenToHeader'
import { API } from '../../../constants'
import Articles from "../../Articles/components/Articles"
import PageLayout from "../../../layouts/PageLayout/PageLayout"
import { browserHistory } from 'react-router'
var jwtDecode = require('jwt-decode')

export const SHOW_ALERT = 'SHOW_ALERT'
export const GET_ALL_ARTICLES = 'GET_ALL_ARTICLES'
export const LOG_IN = 'LOG_IN'
export const ADD_USER = 'ADD_USER'
export const SET_CURRENT_USER = 'SET_CURRENT_USER'
 // export const COUNTER_DOUBLE_ASYNC = 'COUNTER_DOUBLE_ASYNC'

// ------------------------------------
// Actions
// ------------------------------------

export function addUser (username, email, password, passwordConfirm, image) {
  let formData = new FormData()
  formData.append('username', username)
  formData.append('email', email)
  formData.append('password', password)
  formData.append('password_confirmation', passwordConfirm)
  formData.append('image', image)
  return dispatch => {
    axios({
      url: API + 'users',
      method: 'post',
      data: formData
    }).then((response) => {
      localStorage.setItem('jwtToken', response.data.data)
      browserHistory.push('/')
      dispatch({
        type: ADD_USER
      })
    }).catch((errors) => {
      console.log('Error from server', errors.error)
      alert('This user already exists')
    })
  }
}

export function logIn (email, password) {
  return dispatch => {
    axios({
      url: API + '/users/login',
      method: 'post',
      data: {
        email: email,
        password: password
      }
    }).then((response) => {
      const temp = JSON.parse(response.request.response)
      setTokenToHeader(temp.auth_token)
      const tempObject = jwtDecode(temp.auth_token)
      localStorage.setItem('jwtToken', temp.auth_token)
      dispatch(setCurrentUser(tempObject))
      browserHistory.push('/')
    }).catch((errors) => {
      console.log('Error from server in LOGIN', errors)
      alert('Invalid login or password!')
    })
  }
}

export function setCurrentUser (user) {
  return {
    type: SET_CURRENT_USER,
    user
  }
}

export const actions = {
  addUser,
  logIn
}

// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [SHOW_ALERT]: (state) => console.log('---', 'in module articles', state),
  [GET_ALL_ARTICLES]: (state, action) => {
    return Object.assign({}, state, {articles: action.payload})
  },
  [ADD_USER]: (state) => state,
  [LOG_IN]: (state, action) => {
    console.log(action.payload)
    return Object.assign({}, state, { session: action.payload })
  },
  [SET_CURRENT_USER]: (state, action) => {
    return {
      isAuthenticated: !this.isAuthenticated,
      user: action.user
    }
  }
}

// ------------------------------------
// Reducer
// ------------------------------------
// const initialState = articles
const initialState = {
  isAuthenticated: false,
  user: {}
}
export default function userReducer (state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}
