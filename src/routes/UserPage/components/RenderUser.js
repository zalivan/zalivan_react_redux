import React from 'react'
import RenderOneArticleOfUser from './RenderUserArticle'
import PropTypes from 'prop-types'
let _ = require('underscore')
import Pagination from '../../../components/Pagination'

export default class RenderArticlesOfUser extends React.Component {
  constructor () {
    super()
    this.state = {
      exampleItems: this.props || [],
      pageOfItems: []
    }
    this.onChangePage = this.onChangePage.bind(this)
  }
  onChangePage (pageOfItems) {
    this.setState({ pageOfItems: pageOfItems })
  }
  render () {
    const { articles } = this.props
    return (
      <div>
        <br />
        {this.state.pageOfItems.map(article =>
          <RenderOneArticleOfUser key={article.id} article={article} deleteArticle={this.props.articles.deleteArticle}
          />)
        }
        <div className='row'>
          <div className='center'>
            <Pagination items={articles} onChangePage={this.onChangePage} className='center' />
          </div>
        </div>
      </div>
    )
  }
}

// RenderOneArticleOfUser.propTypes = {
//   articles: PropTypes.array.isRequired
// }
