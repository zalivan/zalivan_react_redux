import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Image from 'react-image-file'
import { API } from '../../../constants'
let jwtDecode = require('jwt-decode')
import './UserPage.scss'

export default class RenderOneArticle extends Component {
  render () {
    const article = this.props.article
    return (
      <div data-id={article.id}>
        <h2>
          {article.title}
          <br />
        </h2>
        <h3>
          {article.tag}
        </h3>
        <p>
          {article.body}
        </p>
        <img style={{ height: '150px' }} src={API + `${article.image.url}`} className='center-block' />
        <br />
        <br />
        <div className='news__line' />
        <br />
      </div>
    )
  }
}

// RenderOneArticle.propTypes = {
//   article: PropTypes.object.isRequired
// }
