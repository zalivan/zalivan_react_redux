import React from 'react'
import Image from 'react-image-file'
import PropTypes from 'prop-types'
import './UserPage.scss'
import { API } from '../../../constants'


export default class RenderArticlesOfUser extends React.Component {
  render () {
    const user = this.props.user || {}
    let image = ''
    if (user.image) {
      if (user.image.url === null) {
        image = user.profile_image
      } else {
        image = (API + user.image.url)
      }
    }
    return (
      <div className='user'>
        <img
          src={image}
          className='user__avatar'
        />
        <div className='user__data'>
          <h3>username: <b style={{color: 'brown'}}>{user.username}</b></h3>
          <h2>email: <b style={{color: 'brown'}}>{user.email}</b></h2>
        </div>
      </div>
    )
  }
}

// RenderOneArticleOfUser.propTypes = {
//   articles: PropTypes.array.isRequired
// }
