import React from 'react'
import PropTypes from 'prop-types'
import RenderArticlesOfUser from './RenderUser'
import UserData from './UserData'
import ToogleOpen from '../../../components/ToggleOpen'
import './UserPage.scss'
import { API } from '../../../constants'
import axios from 'axios'

class Users extends React.Component {
  constructor () {
    super()
    this.state = {
      user: {},
      articles: []
    }
  }

  componentWillMount () {
    let arrLocation = +location.pathname.split('/')[2]
    this.props.getUser(arrLocation)
    axios({
      url: API + 'users/' + arrLocation,
      method: 'get',
      data: {
        user_id: arrLocation
      }
    }).then((response) => {
      this.setState({
        user: response.data.data
      })
    }).catch((errors) => {
      console.log('Error from server', errors)
    })
  }

  componentDidMount () {
    let arrLocation = +location.pathname.split('/')[2]
    this.props.getUser(arrLocation)
    this.props.getArticles(arrLocation)
    axios({
      url: API + 'api/v1/articles/' + arrLocation,
      method: 'get',
      data: {
        user_id: arrLocation
      }
    }).then((response) => {
      this.setState({
        articles: response.data.data
      })
    }).catch((errors) => {
      alert(errors.response.message)
    })
  }

  render () {
    const articles = this.state.articles || []
    return (
      <div style={{ margin: '0 auto' }} >
        <UserData user={this.props.user.user_page.user} />
        <h2 style={{color: 'grey', marginBottom: '20px'}}>News</h2>
        <div className='news__line' />
        <RenderArticlesOfUser articles={articles} />
      </div>
    )
  }
}

export default ToogleOpen(Users)

// Users.propTypes = {
//   getArticles: PropTypes.func.isRequired,
//   getUser: PropTypes.func.isRequired
// }
