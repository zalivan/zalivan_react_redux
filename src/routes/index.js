// We only need to import the modules necessary for initial render
import CoreLayout from '../layouts/PageLayout/PageLayout'
import ArticlesRoute from './Articles'
import User from './User'
import Token from './Token'
import AddArticle from './AddArticle'
import UserPage from './UserPage'
import MyAccount from './MyAccount'
import HomeView from './Home'
import Articles from './Articles'

/*  Note: Instead of using JSX, we recommend using react-router
    PlainRoute objects to build route definitions.   */

export const createRoutes = (store) => ({
  path        : '/',
  component   : CoreLayout,
  indexRoute  : Articles(store),
  childRoutes : [
    // Articles(store),
    AddArticle(store),
    User(store),
    UserPage(store),
    Token(store),
    MyAccount(store)
  ]
})

/*  Note: childRoutes can be chunked or otherwise loaded programmatically
    using getChildRoutes with the following signature:

    getChildRoutes (location, cb) {
      require.ensure([], (require) => {
        cb(null, [
          // Remove imports!
          require('./AddArticle').default(store)
        ])
      })
    }

    However, this is not necessary for code-splitting! It simply provides
    an API for async route definitions. Your code splitting should occur
    inside the route `getComponent` function, since it is only invoked
    when the route exists and matches.
*/

export default createRoutes
