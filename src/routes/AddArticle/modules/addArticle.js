import axios from 'axios'
import { browserHistory } from 'react-router'
import { API } from '../../../constants'
// ------------------------------------
// Constants
// ------------------------------------
export const ADD_NEWS = 'ADD_NEWS'

// ------------------------------------
// Actions
// ------------------------------------

/*  This is a thunk, meaning it is a function that immediately
    returns a function for lazy evaluation. It is incredibly useful for
    creating async actions, especially when combined with redux-thunk! */

export function addNews (title, tag, body, userId, username, image) {
  let formData = new FormData()
  formData.append('title', title)
  formData.append('tag', tag)
  formData.append('body', body)
  formData.append('user_id', userId)
  formData.append('username', username)
  formData.append('image', image)
  // axios.post('upload_file', formData, {
  //   headers: {
  //     'Content-Type': 'multipart/form-data'
  //   }
  // })
  return dispatch => {
    axios({
      url: API + '/api/v1/articles/',
      method: 'post',
      data: formData
    }).then((response) => {
      console.log('Send data to server', response.data.data)
      dispatch({
        type: ADD_NEWS
      })
      browserHistory.push('/')
    }).catch((errors) => {
      console.log('Error from server', errors)
    })
  }
}

export const actions = {
  addNews
}

// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [ADD_NEWS] : (state) => state
}

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = 0
export default function counterReducer (state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}
