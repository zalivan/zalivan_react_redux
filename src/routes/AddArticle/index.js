import { injectReducer } from '../../store/reducers'

export default (store) => ({
  path : 'add_article',
  /*  Async getComponent is only invoked when route matches   */
  getComponent (nextState, cb) {
    /*  Webpack - use 'require.ensure' to create a split point
        and embed an async module loader (jsonp) when bundling   */
    require.ensure([], (require) => {
      /*  Webpack - use require callback to define
          dependencies for bundling   */
      const AddArticle = require('./containers/AddArticle').default
      const reducer = require('./modules/addArticle').default

      /*  Add the reducer to the store on key 'counter'  */
      injectReducer(store, { key: 'add_article', reducer })

      /*  Return getComponent   */
      cb(null, AddArticle)

    /* Webpack named bundle   */
    }, 'add_article')
  }
})
