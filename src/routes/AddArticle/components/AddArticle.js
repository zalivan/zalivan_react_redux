import React from 'react'
import PropTypes from 'prop-types'
import './AddArticle.scss'
var jwtDecode = require('jwt-decode')
require('jquery/dist/jquery')
require('popper.js/dist/umd/popper')
require('bootstrap/dist/js/bootstrap')

export default class AddArticle extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      user_id: jwtDecode(localStorage.jwtToken).user_id,
      username: jwtDecode(localStorage.jwtToken).username,
      alert: ''
    }
  }

  handleAddNews = (ev) => {
    const Title = this.refs.createTitle
    const Tag = this.refs.createTag
    const Body = this.refs.createBody
    const Image = this.refs.createImage

    Title.classList.remove('error')
    Tag.classList.remove('error')
    Body.classList.remove('error')

    ev.preventDefault()
    const title = Title.value.trim()
    const tag = Tag.value.trim()
    const body = Body.value.trim()
    const image = Image.files[0]

    if ((title === '') || (title.replace(/\s/g, '') === '')) {
      Title.classList.add('error')
      this.setState({
        alert: 'Title can\'t be empty!'
      })
    } else if ((tag === '') || (tag.replace(/\s/g, '') === '')) {
      Tag.classList.add('error')
      this.setState({
        alert: 'Tag can\'t be empty!'
      })
    } else if ((body === '') || (body.replace(/\s/g, '') === '')) {
      Body.classList.add('error')
      this.setState({
        alert: 'Body can\'t be empty!'
      })
    } else if (!image) {
      console.log(image)
      Image.classList.add('error')
      this.setState({
        alert: 'Please, add image to your news.'
      })
    } else {
      const userId = this.state.user_id
      const username = this.state.username
      this.props.addNews(title, tag, body, userId, username, image)
    }
  }

  render () {
    return (
      <div style={{ margin: '0 auto' }} >
        <br />
        <div className='container'>
          {this.state.alert && <div className='alert alert-danger'>{this.state.alert}</div>}
        </div>
        <br />
        <form encType='multipart/form-data' method='post'>
          <label className='col-lg-9'>Tittle
            <input
              className='form-control col-lg-12'
              onChange={this.handleError}
              ref='createTitle'
            />
          </label>
          <br />
          <label className='col-lg-9'>Tag
            <input
              className='form-control'
              onChange={this.handleError}
              ref='createTag'
            />
          </label>
          <br />
          <label className='col-lg-9'>Body
            <textarea
              className='form-control'
              onChange={this.handleError}
              ref='createBody'
            />
          </label>
          <br />
          <label>Image
            <br />
            <input
              type='file'
              name='photo'
              className='form-control'
              ref='createImage'
              onChange={this.changeImage}
              accept='image/*'
            />
          </label>
          <br />
          <button
            className='btn btn-success'
            type='submit'
            onClick={this.handleAddNews}>
            Add news
          </button>
        </form>
      </div>
    )
  }

  handleError = (ev) => {
    ev.target.classList.remove('error')
    this.setState({
      alert: ''
    })
  }
}
AddArticle.propTypes = {
  addNews: PropTypes.func.isRequired
}
