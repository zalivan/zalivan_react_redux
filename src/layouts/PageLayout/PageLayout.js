import React from 'react'
import { IndexLink, Link, browserHistory } from 'react-router'
import PropTypes from 'prop-types'
import './PageLayout.scss'
let jwtDecode = require('jwt-decode')

export default class PageLayout extends React.Component {
  closeSession = () => {
    localStorage.clear()
    browserHistory.push('/user')
  }
  render () {
    let { children } = this.props
    let userObject = {}
    if (localStorage.jwtToken) {
      userObject = jwtDecode(localStorage.jwtToken)
    }
    return (
      <div className='text-center'>
        <nav className='navbar navbar-dark naviration' style={{ backgroundColor: '#303030' }}>
          <div className='container'>
            <IndexLink to='/' className='nav-link' activeClassName='page-layout__nav-item--active'>All news</IndexLink>
            {localStorage.jwtToken &&
            <Link to='/add_article' className='nav-link' activeClassName='page-layout__nav-item--active'>Add news</Link>
            }
            <Link to={!localStorage.jwtToken ? '/user_page' :
              '/my_account/' + userObject.user_id} className='nav-link' activeClassName='page-layout__nav-item--active'>
              {localStorage.jwtToken && jwtDecode(localStorage.jwtToken).username}
            </Link>
            {!localStorage.jwtToken &&
            <Link to='/user' className='nav-link' activeClassName='page-layout__nav-item--active'>Log in</Link>}
            {localStorage.jwtToken &&
            <button className='button_logout' onClick={this.closeSession}>Log out</button>}
          </div>
        </nav>
        <div className='page-layout__viewport'>
          {children}
        </div>
      </div>
    )
  }
}

PageLayout.propTypes = {
  children: PropTypes.node,
}
